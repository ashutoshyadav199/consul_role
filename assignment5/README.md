# *Installation of Consul tool on Ubuntu and RedHat with Ansible role*
## Before install consul we need to add key, after that we need to add repository for installation on consul, then update repository then we can install consul tool.
---
## *Output*

![playbook_output1](/uploads/86419207861d32f31bb44bf619152040/playbook_output1.png)

![playbook_output2](/uploads/1e0090db18540030e45a426b0c3b53c8/playbook_output2.png)

![ubuntu](/uploads/108df668f5facbf90f38b607fa7e8e2f/ubuntu.png)

![redhat](/uploads/19460345107c5cd5ad8fd78edb567d66/redhat.png)

---
# *Thank You*
## Author
## [Ashutosh Yadav](https://gitlab.com/ashutoshyadav199)